package leetcode;

import com.carrotsearch.junitbenchmarks.BenchmarkOptions;
import com.carrotsearch.junitbenchmarks.BenchmarkRule;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import utils.ArrayUtils;

/**
 * @author Andrei Podymov <arpodymov@gmail.com>
 */
public class ReverseStringBenchmark {

    private static char testData[];
    private static char testCase[];

    @Rule
    public TestRule benchmarkRule = new BenchmarkRule();

    static {
        testData = ArrayUtils.generateRandomArray(50000000);
    }

    @Before
    public void before() {
        testCase = testData.clone();
    }

    @Test
    @BenchmarkOptions(benchmarkRounds = 5, warmupRounds = 2)
    public void iterativeBenchmark() {
        ReverseString.reverseStringIterative(testCase);
        String expected = new StringBuilder(new String(testData)).reverse().toString();
        Assert.assertEquals(expected, new String(testCase));
    }

    @Test
    @BenchmarkOptions(benchmarkRounds = 1, warmupRounds = 0)
    public void recursiveBenchmark() {
        ReverseString.reverseStringRecursive(testCase, 0 , testCase.length - 1);
        String expected = new StringBuilder(new String(testData)).reverse().toString();
        Assert.assertEquals(expected, new String(testCase));
    }

    @Test
    @BenchmarkOptions(benchmarkRounds = 5, warmupRounds = 2)
    public void twoPtrBenchmark() {
        ReverseString.reverseStringTwoPtr(testCase);
        String expected = new StringBuilder(new String(testData)).reverse().toString();
        Assert.assertEquals(expected, new String(testCase));
    }
}
