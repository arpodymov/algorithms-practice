package leetcode;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

/**
 * @author Andrei Podymov <arpodymov@gmail.com>
 */
public class ReverseStringTest {

    @Test
    public void test0() {
        char testData[] = {};
        char expected[] = {};

        char testCase[] = testData.clone();
        ReverseString.reverseStringIterative(testCase);
        assertArrayEquals(expected, testCase);

        testCase = testData.clone();
        ReverseString.reverseStringRecursive(testCase, 0, testCase.length - 1);
        assertArrayEquals(expected, testCase);

        testCase = testData.clone();
        ReverseString.reverseStringTwoPtr(testCase);
        assertArrayEquals(expected, testCase);
    }

    @Test
    public void test1() {
        char testData[] = {'a', 'b', 'c', 'd', 'e'};
        char expected[] = {'e', 'd', 'c', 'b', 'a'};

        char testCase[] = testData.clone();
        ReverseString.reverseStringIterative(testCase);
        assertArrayEquals(expected, testCase);

        testCase = testData.clone();
        ReverseString.reverseStringRecursive(testCase, 0, testCase.length - 1);
        assertArrayEquals(expected, testCase);

        testCase = testData.clone();
        ReverseString.reverseStringTwoPtr(testCase);
        assertArrayEquals(expected, testCase);
    }

    @Test
    public void test2() {
        char testData[] = {'a', 'b', 'c', 'd'};
        char expected[] = {'d', 'c', 'b', 'a'};

        char testCase[] = testData.clone();
        ReverseString.reverseStringIterative(testCase);
        assertArrayEquals(expected, testCase);

        testCase = testData.clone();
        ReverseString.reverseStringRecursive(testCase, 0, testCase.length - 1);
        assertArrayEquals(expected, testCase);

        testCase = testData.clone();
        ReverseString.reverseStringTwoPtr(testCase);
        assertArrayEquals(expected, testCase);
    }

    @Test
    public void test3() {
        char testData[] = {'a', 'b', 'B', 'a'};
        char expected[] = {'a', 'B', 'b', 'a'};

        char testCase[] = testData.clone();
        ReverseString.reverseStringIterative(testCase);
        assertArrayEquals(expected, testCase);

        testCase = testData.clone();
        ReverseString.reverseStringRecursive(testCase, 0, testCase.length - 1);
        assertArrayEquals(expected, testCase);

        testCase = testData.clone();
        ReverseString.reverseStringTwoPtr(testCase);
        assertArrayEquals(expected, testCase);
    }
}
