package codeforces;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class RegistrationSystemTest {

    @Test
    public void test1() {
        List<String> testData = Arrays.asList("a", "b", "c");
        List<String> expected = Arrays.asList("OK", "OK", "OK");
        Assert.assertEquals(RegistrationSystem.solve(testData), expected);
    }

    @Test
    public void test2() {
        List<String> testData = Arrays.asList("a", "b", "b");
        List<String> expected = Arrays.asList("OK", "OK", "b1");
        Assert.assertEquals(RegistrationSystem.solve(testData), expected);
    }

    @Test
    public void test3() {
        List<String> testData = Arrays.asList("c", "c", "c", "d");
        List<String> expected = Arrays.asList("OK", "c1", "c2", "OK");
        Assert.assertEquals(RegistrationSystem.solve(testData), expected);
    }
}
