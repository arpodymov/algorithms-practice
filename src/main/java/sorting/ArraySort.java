package sorting;

public interface ArraySort <T> {
    void sort(T[] arr);
}
