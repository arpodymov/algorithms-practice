package sorting;

import com.carrotsearch.junitbenchmarks.BenchmarkOptions;
import com.carrotsearch.junitbenchmarks.BenchmarkRule;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import utils.ArrayUtils;

import static utils.ArrayUtils.isArraySorted;

/**
 * @author Andrei Podymov <arpodymov@gmail.com>
 */
@Slf4j
public class SortingBenchmark {

    private static Integer[] testDataArr;
    private static Integer[] testCaseArr;

    private static final int n = 200000;
    private static final int min = -5000000;
    private static final int max = 5000000;

    @Rule
    public TestRule benchmarkRule = new BenchmarkRule();

    static {
        log.info("Test dataset ready with item count: {} in range: [{}, {}]", n, min, max);
        testDataArr = ArrayUtils.generateRandomArray(n, min, max);
    }

    @Before
    public void before() {
        testCaseArr = testDataArr.clone();
    }

    @Test
    @BenchmarkOptions(benchmarkRounds = 1, warmupRounds = 0)
    public void insertionSortBenchmark() {
        new InsertionSort().sort(testCaseArr);
        Assert.assertTrue(isArraySorted(testCaseArr));
    }

    @Test
    @BenchmarkOptions(benchmarkRounds = 1, warmupRounds = 0)
    public void mergeSortBenchmark() {
        new MergeSort().sort(testCaseArr);
        Assert.assertTrue(isArraySorted(testCaseArr));
    }

    @Test
    @BenchmarkOptions(benchmarkRounds = 1, warmupRounds = 0)
    public void selectionSortBenchmark() {
        new SelectionSort().sort(testCaseArr);
        Assert.assertTrue(isArraySorted(testCaseArr));
    }

    @Test
    @BenchmarkOptions(benchmarkRounds = 1, warmupRounds = 0)
    public void countingSortBenchmark() {
        new CountingSort().sort(testCaseArr);
        Assert.assertTrue(isArraySorted(testCaseArr));
    }

}
